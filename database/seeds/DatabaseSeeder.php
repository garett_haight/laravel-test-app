<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		DB::table('users')->insert(
 		[
            'name' => "git",
            'email' => "git@mcclainconcepts.com",
            'password' => bcrypt('LaravelTestPW'),
        ]);
		
		DB::table('users')->insert(
		[
            'name' => "garett",
            'email' => "garett.haight@gmail.com",
            'password' => bcrypt('LaravelTestPW'),
        ]
		);
		
		
    }
}
